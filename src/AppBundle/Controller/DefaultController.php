<?php

namespace AppBundle\Controller;

use dsarhoya\DSYXLSBundle\Services\ExcelService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    private $excelService;

    public function __construct(ExcelService $excelService)
    {
        $this->excelService = $excelService;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $collection = [
            ['row_data' => ['a', 'b']],
            ['row_data' => ['c', 'd']],
        ];

        $file = $this->excelService->getExcelFileFromCollection(['l', 'i'], $collection);

        if (null !== $file) {
            return $this->excelService->returnExcelFile($file, 'nombre', ExcelService::FILE_TYPE_EXCEL);
        }

        return $this->render('default/index.html.twig', [
        ]);
    }

    /**
     * @Route("/sheets", name="sheets")
     */
    public function sheetsAction(Request $request)
    {
        $collection = [
            ['row_data' => ['a1111', 'b2222']],
            ['row_data' => ['c1111', 'd2222']],
        ];

        $file = $this->excelService->getExcelFileFromCollection(['header1', 'header2'], $collection, null, null, ['sheetName' => 'General']);

        $collection = [
            ['row_data' => ['e1111', 'f2222']],
            ['row_data' => ['g1111', 'h2222']],
        ];

        $pendingSheet = $this->excelService->createSheet($file, 'Pendientes');
        $this->excelService->setDataOnSheet($pendingSheet, ['header3', 'header4'], $collection, null);

        if (null !== $file) {
            return $this->excelService->returnExcelFile($file, 'nombre', ExcelService::FILE_TYPE_EXCEL);
        }

        return $this->render('default/index.html.twig', [
        ]);
    }
}

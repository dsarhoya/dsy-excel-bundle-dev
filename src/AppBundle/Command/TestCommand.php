<?php

namespace AppBundle\Command;

use dsarhoya\DSYXLSBundle\Services\ExcelService;
use dsarhoya\DSYXLSBundle\XLS\Loader\ExcelLoader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Constraints\NotBlank;

class TestCommand extends ContainerAwareCommand
{
    private $excelService;

    public function __construct(ExcelService $excelService)
    {
        $this->excelService = $excelService;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('test')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $loader = $this->excelService->getExcelLoader();

        $loader->addCell(new \dsarhoya\DSYXLSBundle\XLS\Loader\ColumnOptions([
            'name' => 'a',
        ]));
        $loader->addCell(new \dsarhoya\DSYXLSBundle\XLS\Loader\ColumnOptions([
            'name' => 'b',
            'constraints' => [new NotBlank()],
        ]));

        $file = __DIR__.DIRECTORY_SEPARATOR.'Data'.DIRECTORY_SEPARATOR.'ruts_400000.xlsx';

        // $res = $loader->load($file);

        // dump($res);

        (new SymfonyStyle($input, $output))->note('Empiezo a mostrar resultados');

        $loader->setHasHeaders(true);

        // $loader->startsAtIndex(3);
        // $loader->setStopsOnError(true);
        // $loader->setMaxRows(0);

        $rows = $loader->getMaxRows($file);
        $estimation = $rows === ExcelLoader::COUNT_MAX_VALUE;
        
        dump($rows);
        die;
        $res = $loader->load($file);
        // dump($res);die;

        $total = 0;
        foreach ($res as $data) {
            ++$total;
            // dump($data);
            dump($total.' -> '.(memory_get_peak_usage() / 1024 / 1024));
        }

        dump($loader->getErrors());

        (new SymfonyStyle($input, $output))->success('Listo!');
    }
}
